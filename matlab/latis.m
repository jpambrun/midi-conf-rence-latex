%% add path
currmfile = mfilename('fullpath');
currPath = currmfile(1:end-length(mfilename()));
addpath([currPath 'dep']);
addpath([currPath 'dep' filesep 'latexfigure']);
setenv('LD_LIBRARY_PATH',getenv('PATH')); % règle un petit bug 
                                          % de matlab sous linux..

%%

xx = 0:.01:10;
y1 = sin(xx.^2);
y2 = cos(xx);
plot(xx,y1,xx,y2);
title('Exemple de figure','Interpreter','Latex') 
xlabel('Temps($s$)','Interpreter','Latex') 
ylabel('Tension ($v$)','Interpreter','Latex') 
legend('$\rm{sin}(x^2)$', '$\rm{cos}(x)$') 

%set figure size
set(gcf, 'units', 'inches', 'pos', [0 0 3.5 3.5])
set(gcf, 'PaperSize', [3.5 3.5]);

%set figure background
set(gcf, 'Color', [1 1 1]);                       
set(gca, 'Color', [1 1 1]);

%set render to painter
set(gcf, 'Renderer', 'painters');

%set font size
set(findall(gcf,'type','text'),'fontSize',10)
set(gca,'FontSize',10)

% from https://code.google.com/p/latexfigure/
latexPackages = ['\usepackage{charter}\n' ...
                 '\renewcommand{\sfdefault}{bch}\n'...
                 ' \renewcommand{\rmdefault}{bch}\n'];   
latexfigure(gcf,'mafigure','pdf',...
                'packages', latexPackages,...  
                'distpath','',...
                'dpi',300,... 
                'fixLines',true);


